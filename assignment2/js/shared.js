// shared.js contains all the classes and methods, as well as all the functiosn that start when the application is opened.
"use strict";

// Keys for local storage
const BOOKING_INDEX_KEY = "bookingIndex"
const APP_DATA_KEY = "taxiAppData"

class Booking {
    // Takes the date, location, destination, layovers, stopCount, distance, fare, and type, and assigns them to the appropriate attributes.
    constructor(date, location, destination, layovers, stopCount, distance, fare, type) {
        this._date = date
        this._location = location
        this._destination = destination
        this._layovers = layovers
        this._stopCount = stopCount
        this._distance = distance
        this._fare = fare
        this._type = type
        this._complete = false
    }

    get date() {
        return this._date
    }
    get location() {
        return this._location
    }
    get destination() {
        return this._destination
    }
    get layovers() {
        return this._layovers
    }
    get stopCount() {
        return this._stopCount
    }
    get distance() {
        return this._distance
    }
    get fare() {
        return this._fare
    }
    get type() {
        return this._type
    }
    get complete() {
        return this._complete
    }

    // Method fromData() assigns the booking data object to the appropriate attributes.
    fromData(booking) {
        this._date = booking._date
        this._location = booking._location
        this._destination = booking._destination
        this._layovers = booking._layovers
        this._stopCount = booking._stopCount
        this._distance = booking._distance
        this._fare = booking._fare
        this._type = booking._type
        this._complete = booking._complete
    }
}

class List {
    // Creates a new array which will hold all the booking data objects.
    constructor() {
        this._bookings = []
    }

    get bookings() {
        return this._bookings
    }

    // Method calculateDistance() takes an array of coordinates to calculate the distance between each layover and destination.
    calculateDistance(coordinates) {
        let distance = 0
    
        // For loop that iterates over two sets of coordinates at a time.
        for (let i = 0; i < coordinates.length; i++) {
            let j = i + 1

            // via: https://www.movable-type.co.uk/scripts/latlong.html
            if (j < coordinates.length) {
                let lat1 = coordinates[i][1]
                let lon1 = coordinates[i][0]
                let lat2 = coordinates[j][1]
                let lon2 = coordinates[j][0]

                let R = 6371; // kilometres
                let φ1 = lat1 * Math.PI/180; // φ, λ in radians
                let φ2 = lat2 * Math.PI/180;
                let Δφ = (lat2-lat1) * Math.PI/180;
                let Δλ = (lon2-lon1) * Math.PI/180;

                let a = Math.sin(Δφ/2) * Math.sin(Δφ/2) + Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ/2) * Math.sin(Δλ/2);
                let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

                let d = R * c; // in kilometres
                distance += d
            }
        }
        console.log(distance)
        return distance
    }

    // Method calculateFare() takes the date, type, and distance in order to calculate the fare.
    calculateFare(date, type, distance) {
        // Fare structure that was given by the client.
        const flatRate = 4.2
        const distanceRate = 1.622
        const vehicleLevy = 1.1
        let additionalLevy = 0
        let hour = date.getHours()
    
        // If statements to determine the price depending on the chosen vehicle. Default is 0 for sedan.
        if (type === "SUV") {
            additionalLevy = 3.5
        } else if (type === "Van") {
            additionalLevy = 6
        } else if (type === "Minibus") {
            additionalLevy = 10
        }

        // If statements to determine whether to add the night levy or not depending on the time of the booking.
        if (hour <= 9 || hour >= 17) {
            let nightLevy = (flatRate + distance*distanceRate + additionalLevy + vehicleLevy)* 1 / 5
            let fare = flatRate + distance*distanceRate + additionalLevy + vehicleLevy + nightLevy
            console.log(fare)
            return fare
        } else {
            let fare = flatRate + distance*distanceRate + additionalLevy + vehicleLevy
            console.log(fare)
            return fare
        }
    }

    // Method book() takes the date, location, destination, layovers, stopcount, distance, fare, and type, to create a new booking class instance. 
    book(date, location, destination, layovers, stopCount, distance, fare, type) {
        let booking = new Booking(date, location, destination, layovers, stopCount, distance, fare, type)
        this._bookings.unshift(booking)
    }

    // Method cancelBooking() takes the bookingIndex and removes it from local storage when called.
    cancelBooking(bookingIndex) {
        this._bookings.splice(bookingIndex, 1)
    }

    // Method getBooking() takes the bookingIndex and returns the booking with that index.
    getBooking(bookingIndex) {
        return this._bookings[bookingIndex]
    }

    // Method fromData() takes the list instance from local storage.
    fromData(list) {
        // For loop to iterate over the booking data objects from local storage and pushes them to the list bookings array.
        for (let i = 0; i < list._bookings.length; i++) {
            let newBooking = new Booking()
            newBooking.fromData(list._bookings[i])
            this._bookings.push(newBooking)
        }
    }
}

// This function uses OpenCage API to convert strings into coordinates. 
// Due to the limitations of the API, we had to set a delay at 1 request per 2 seconds or else the array of coordinates will be out of order.
function getCoordinates(trip) {
    let coordinates = []

    // https://opencagedata.com/tutorials/geocode-in-javascript
    for (let i = 0; i < trip.length; i++) {
        setTimeout(() => {
        let request_url = 'https://api.opencagedata.com/geocode/v1/json?key=0e15987e0dcf4adabce9a96e8445ac77&q=' + encodeURIComponent(trip[i]) + '&no_annotations=1';

        let request = new XMLHttpRequest();
        request.open('GET', request_url, true);

        request.onload = function() {
            if (request.status === 200) { 
                let data = JSON.parse(request.responseText);
                let longitude = data.results[0].geometry.lng
                let latitude = data.results[0].geometry.lat
                coordinates.push([longitude, latitude])
                console.log(coordinates)
            } else if (request.status <= 500) { 
                console.log("unable to geocode! Response code: " + request.status);
                let data = JSON.parse(request.responseText);
                 console.log('error msg: ' + data.status.message);
             } else {
                console.log("server error");
            }
        };

        request.onerror = function() {
            console.log("unable to connect to server");
        };

        request.send();
        }, i * 2000)
    }
    return coordinates
}

// These functions were taken from Assignment 1.b as they interact the same way with local storage.

// Function checkLocalStorageDataExist takes the app data key to check if there is existing data.
function checkLocalStorageDataExist(key) {
    // Returns true if data exists.
    if (localStorage.getItem(key)) {
        return true;
    }
    // Returns false if data doesn't exist.
    return false;
}

// Function updateLocalStorageData takes the app data key and respective session data. Saves the session data to local storage and sets the key.
function updateLocalStorageData(key, data) {
    let jsonData = JSON.stringify(data);
    localStorage.setItem(key, jsonData);
}

// Function getLocalStorageData takes the app data key and loads the existing session data.
function getLocalStorageData(key) {
    let jsonData = localStorage.getItem(key);
    let data = jsonData;
    // Attempt to load the session data.
    try {
        data = JSON.parse(jsonData);
        // Logs an error if the task fails.
    } catch(e) {
        console.error(e);
        // Returns the data if the task succeeds.
    } finally {
        return data;
    }
}

// Code that runs when the file is loaded.
let taxiList = new List();
// Creates a new list instance, load from local storage if data exists.
if (checkLocalStorageDataExist(APP_DATA_KEY)) {
    let data = getLocalStorageData(APP_DATA_KEY);
    taxiList.fromData(data);
    // Saves new instance to the local storage if data does not exist.
} else {
    updateLocalStorageData(APP_DATA_KEY, taxiList);
}