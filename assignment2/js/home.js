// home.js has all the functions required to run home.html. It is used by the user to create new bookings.
"use strict";

// Function lickClock() starts when the page is loaded and displays a live clock.
function liveClock() {
    let date = new Date()
    let hours = date.getHours()
    let minutes = date.getMinutes()
    let seconds = date.getSeconds()

    // Ensures that the hours, minutes, and seconds that are < 10 have a leading zero.
    function zero(i) {
        if (i < 10) {
            i = "0" + i
        }
        return i
    }

    // If hours, minutes, or seconds is < 10, add a leading zero so that the time format is correct.
    hours = zero(hours)
    minutes = zero(minutes)
    seconds = zero(seconds)

    // Takes the variables hours, minutes, and seconds and outputs as a string to the element with id currentTime to output a live clock.
    document.getElementById("currentTime").innerHTML = `${hours}:${minutes}:${seconds}`
}

// Function book() takes the input values from HTML.
function book() {
    if (confirm ("Are you sure you would like to make this booking?")) {
        let location = document.getElementById("location").value
        let destination = document.getElementById("destination").value
        let layoverString = document.getElementById("layovers").value
        let type = document.getElementById("type").value
       
        let layoverArray = []
        let trip = []

        let distance = 0
        let fare = 0
        let date = new Date()

        // Checks if all the inputs are valid. Returns false if they are invalid.
        if (location.length === 0) {
            alert("Please enter a location!")
            return false
        }
        if (destination.length === 0) {
            alert("Please enter a destination!")
            return false
        }
        if (type.length === 0) {
            alert("Please select a valid taxi type!")
            return false
        }

        // Checks if layover input is empty. If not empty, converts the string into an array.
        if (layoverString) {
            layoverArray = layoverString.split(",")   
        }

        // For loop to push the values from the array to a new array.
        for (let i = 0; i < layoverArray.length; i++) {
            trip.push(layoverArray[i])
        }

        // Unshift the location string to the start of the array and push the destination string to the end of the array.
        trip.unshift(location)
        trip.push(destination)

        // Count the number of stops by counting the amount of elements in the array minus the starting location.
        let stopCount = trip.length - 1

        // Calls function getCoordinates() in shared.js and assings the value to coordinates.
        let coordinates = getCoordinates(trip)

        // Due to Chromiums JavaScript console being lazy, we are forced to use delays so that the values get evaluated and assigned properly.
        // Calls method calculateDistance() and calculateFare() and assigns it to variable distance and fare respectively.
        setTimeout(() => {distance = taxiList.calculateDistance(coordinates)}, trip.length * 2000 + 3000)
        setTimeout(() => {fare = taxiList.calculateFare(date, type, distance)}, trip.length * 2000 + 6000)


        // Calls method book() and updates the local storage.
        setTimeout(() => {taxiList.book(date, location, destination, layoverArray, stopCount, distance, fare, type)}, trip.length * 2000 + 9000)
        setTimeout(() => {updateLocalStorageData(APP_DATA_KEY, taxiList)}, trip.length * 2000 + 9000)

        // Alerts the user that they have made the booking and shows the summary after a set amount of time.
        alert("You have successfully made a booking. Showing Summary in " + (trip.length * 2000 + 9000)/1000 + " Seconds...")
        setTimeout(() => {window.location = "summary.html"}, trip.length * 2000 + 9000)
    }
}

// Anonymouse function that runs when the web page is loaded.
window.onload = function () {
    // Call function liceClock() and sets an interval to call the function every 1000 milliseconds.
    liveClock()
    setInterval(liveClock, 1000)

}

// via: https://developer.mozilla.org/en-US/docs/Web/API/Geolocation_API/Using_the_Geolocation_API
// via: https://docs.mapbox.com/mapbox-gl-js/example/simple-map/
// via: https://docs.mapbox.com/mapbox-gl-js/api/markers/#popup
// via: https://docs.mapbox.com/mapbox-gl-js/example/geojson-line/
mapboxgl.accessToken = 'pk.eyJ1IjoiZWlscmVpdmF4IiwiYSI6ImNrdDAweDd2cDAwaXMydnA5cGNiaTFod2gifQ.AdGGJsJwWkL-r-s97TZu3A'

// Determines if able to detect user's current location. If able to, returns user's location in coordinates. Else, returns error.
if("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition(success, error)
} else {
    error()
}

// Function that runs when user's current location is available.
function success(position) {
    let currentCoordinates = [position.coords.longitude, position.coords.latitude]
    
    // Creates a new map and centers on the user's current location.
    let map = new mapboxgl.Map({
    container: 'map',
     style: 'mapbox://styles/mapbox/streets-v11',
        zoom: 15,
        center: currentCoordinates
    })
    // Creates a new popup at the user's current location.
    new mapboxgl.Popup({offset: 36})
        .setLngLat(currentCoordinates)
        .setHTML("Current Location")
        .addTo(map)
    // Creates a new marker at the user's current location.
    new mapboxgl.Marker()
        .setLngLat(currentCoordinates)
        .addTo(map)
}

// Function that runs when user's current location is unavailable.
function error() {
    alert("Geolocation is not supported by your browser!")
}