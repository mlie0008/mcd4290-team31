// viewspecific.js runs when viewspecific.html is opened and displays the whole booking details with a map of the route.
"use strict";

// Loads the booking index from local storage and assigns them to the variable bookingIndex.
let bookingIndex = localStorage.getItem(BOOKING_INDEX_KEY)

// Takes the booking index and calls method getBooking() to get the booking data object.
let bookingInfo = taxiList.getBooking(bookingIndex);

// Assignes the HTML elements with the appropriate attributes.
document.getElementById("title").innerHTML = "Trip from " + bookingInfo._location + " to " + bookingInfo._destination
document.getElementById("location").innerHTML = bookingInfo._location
document.getElementById("destination").innerHTML = bookingInfo._destination
document.getElementById("stopCount").innerHTML = bookingInfo._stopCount
document.getElementById("distance").innerHTML = bookingInfo._distance.toFixed(2) + " km"
document.getElementById("fare").innerHTML = "AU$" + bookingInfo._fare.toFixed(2)
document.getElementById("type").innerHTML = bookingInfo._type

// Assigns a cancel button if the bookingInfo complete is not = true.
if (!bookingInfo._complete) {
    document.getElementById("cancel").innerHTML = 
    '<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" onclick="cancel(BOOKING_INDEX_KEY)">' 
    + 'Cancel Booking'
    + '</button>'
}

// Function cancel() takes the bookingIndex and removes the student from local storage.
function cancel(bookingIndex) {
    // Confirms with the user whether they want to cancel the booking.
    if (confirm("Are you sure you would like to cancel this booking?\nWARNING: THIS ACTION CANNOT BE UNDONE")) {
        // Calls method cancelBooking() and updates local storage.
        taxiList.cancelBooking(bookingIndex)
        updateLocalStorageData(APP_DATA_KEY, taxiList)
    }
    // Redirects the user to view.html.
    window.location = "view.html"
}

// Creating a new array that will contain the location, layovers, and destination.
let trip = []
let layovers = bookingInfo._layovers

for (let i = 0; i < layovers.length; i++) {
    trip.push(layovers[i])
}
trip.unshift(bookingInfo._location)
trip.push(bookingInfo._destination)

// Alerts the user that the map will be loaded after a certain amount of time.
let coordinates = getCoordinates(trip)
alert("Loading map in " + (trip.length * 2000 + 3000)/1000 + " seconds. Please wait...")

// Creates a map, popup, and markers with a delay to compensate for the calculations of the distance and fare.
setTimeout(() => {
    // via: https://developer.mozilla.org/en-US/docs/Web/API/Geolocation_API/Using_the_Geolocation_API
    // via: https://docs.mapbox.com/mapbox-gl-js/example/simple-map/
    // via: https://docs.mapbox.com/mapbox-gl-js/api/markers/#popup
    // via: https://docs.mapbox.com/mapbox-gl-js/example/geojson-line/
    mapboxgl.accessToken = 'pk.eyJ1IjoiZWlscmVpdmF4IiwiYSI6ImNrdDAweDd2cDAwaXMydnA5cGNiaTFod2gifQ.AdGGJsJwWkL-r-s97TZu3A'

    // Creates a new map and centers on the starting location.
    let map = new mapboxgl.Map({
    container: 'map',
     style: 'mapbox://styles/mapbox/streets-v11',
        zoom: 15,
        center: coordinates[0]
    })
    // Creates a new popup for the location, destination, and each layover.
    for (let i = 0; i < trip.length; i++) {
        new mapboxgl.Popup({offset: 36})
            .setLngLat(coordinates[i])
            .setHTML(trip[i])
            .addTo(map)

        // Creates a new marker for the location, destination, and each layover.
        new mapboxgl.Marker()
            .setLngLat(coordinates[i])
            .addTo(map)
    }
    
    // Add lines between the location, stopovers, and destination with a delay to allow the css to load.
    setTimeout(() => {
        map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': coordinates
                }
            }
        })
        map.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': '#888',
                'line-width': 8
            }
        })
    }, 1000)
}, trip.length * 2000 + 3000)