// view.js has all the functions required to run view.html. Including the updating of the page to show the booking data from newest to oldest, and categorises them by completed or not.
"use strict";

// Function view() takes the bookingIndex to find the specific booking instance.
function view(bookingIndex) {
    localStorage.setItem(BOOKING_INDEX_KEY, bookingIndex)
    window.location = "viewspecific.html"
}

// Function markComplete() takes the bookingIndex and changes the complete status to true.
function markComplete(bookingIndex) {
    // Confirms with the user whether they want to mark this booking as complete.
    if (confirm("Are you sure you would like to mark this booking as complete?")) {
        // Changes the complete to true.
        taxiList._bookings[bookingIndex]._complete = true
        // Saves the new data to local storage.
        updateLocalStorageData(APP_DATA_KEY, taxiList)
    }
    // Calls the function displayBookings() to update the page.
    displayBookings(taxiList)
}

// function displayBookings()  takes the list instance.
function displayBookings(data) {
    let output1 = ""
    let output2 = ""

    // For loop to iterage over each booking instance.
    for (let i = 0; i < data._bookings.length; i++) {
        let bookingDate = new Date(data._bookings[i]._date)

        let day = bookingDate.getDate()
        let month = bookingDate.getMonth() + 1
        let year = bookingDate.getFullYear()
        let hours = bookingDate.getHours()
        let minutes = bookingDate.getMinutes()
        let seconds = bookingDate.getSeconds()

        // Ensures that the hours, minutes, and seconds that are < 10 have a leading zero.
        function zero(i) {
            if (i < 10) {
                i = "0" + i
            }
            return i
        }
    
        // If hours, minutes, or seconds is < 10, add a leading zero so that the time format is correct.
        hours = zero(hours)
        minutes = zero(minutes)
        seconds = zero(seconds)
 
        // Takes the variables day, month, year, hours, minutes, and seconds and sets it to variable date.
        let date = `${day}/${month}/${year}, ${hours}:${minutes}:${seconds}`

        // Sorts the booking data by whether complete is = true or not. If complete, will not produce a markComplete button.
        if (!data._bookings[i]._complete) {
            output1 +=
            `<li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">
                    <span>Trip from ${data._bookings[i]._location} to ${data._bookings[i]._destination}</span>
                </span>
                <span class="mdl-list__item-secondary-content">
                    <a class="mdl-list__item-secondary-action" onclick="view(${i})"><i
                        class="material-icons">info</i>
                    </a>
                </span>
                <span class="mdl-list__item-secondary-content">
                    <a class="mdl-list__item-secondary-action" onclick="markComplete(${i})"><i
                        class="material-icons">done</i>
                    </a>
                </span>
                <li>
                    <span class="mdl-list__item-secondary-content">
                        <span>Booking Date: ${date}</span>
                    </span>
                </li>
                <li>
                    <span class="mdl-list__item-secondary-content">
                        <span>Pick-up Point: ${data._bookings[i]._location}</span>
                    </span>
                </li>
                <li>
                    <span class="mdl-list__item-secondary-content">
                        <span>Desination: ${data._bookings[i]._destination}</span>
                    </span>
                </li>
                <li>
                    <span class="mdl-list__item-secondary-content">
                        <span>No. of Stops: ${data._bookings[i]._stopCount}</span>
                    </span>
                </li>
                <li>
                    <span class="mdl-list__item-secondary-content">
                        <span>Est. Distance: ${data._bookings[i]._distance.toFixed(2)} km</span>
                    </span>
                </li>
                <li>
                    <span class="mdl-list__item-secondary-content">
                        <span>Est. Fare: AU$${data._bookings[i]._fare.toFixed(2)}</span>
                    </span>
                </li>
            </li>`
        } else {
            output2 +=
            `<li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">
                    <span>Trip from ${data._bookings[i]._location} to ${data._bookings[i]._destination}</span>
                </span>
                <span class="mdl-list__item-secondary-content">
                    <a class="mdl-list__item-secondary-action" onclick="view(${i})"><i
                        class="material-icons">info</i>
                    </a>
                </span>
                <li>
                    <span class="mdl-list__item-secondary-content">
                        <span>Booking Date: ${date}</span>
                    </span>
                </li>
                <li>
                    <span class="mdl-list__item-secondary-content">
                        <span>Pick-up Point: ${data._bookings[i]._location}</span>
                    </span>
                </li>
                <li>
                    <span class="mdl-list__item-secondary-content">
                        <span>Desination: ${data._bookings[i]._destination}</span>
                    </span>
                </li>
                <li>
                    <span class="mdl-list__item-secondary-content">
                        <span>No. of Stops: ${data._bookings[i]._stopCount}</span>
                    </span>
                </li>
                <li>
                    <span class="mdl-list__item-secondary-content">
                        <span>Est. Distance: ${data._bookings[i]._distance.toFixed(2)} km</span>
                    </span>
                </li>
                <li>
                    <span class="mdl-list__item-secondary-content">
                        <span>Est. Fare: AU$${data._bookings[i]._fare.toFixed(2)}</span>
                    </span>
                </li>
            </li>`    
        }
    }
    document.getElementById("queueContent").innerHTML = "<h4>Current Bookings</h4>" + output1 + "<h4>Past Bookings</h4>" + output2
}

// Anonymous function that runs when the web page is loaded.
window.onload = function () {
    // Call function displayBookings to update the page with the list instance.
    displayBookings(taxiList)
}