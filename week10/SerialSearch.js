/* Write your function for implementing the Serial Search Algorithm here */

function serialSearchTest(array){
    for (let centerIndex = 0; centerIndex < array.length; centerIndex++) {
        if (array[centerIndex].emergency)
            return array[centerIndex].address;
    }
    return null;
}
